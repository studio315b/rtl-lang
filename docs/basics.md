# RTL Basics

RTL files are newline-delimited, which means commands are expected to end with a newline.
There are a handful of exceptions to this rule (you don't need a trailing newline, and a newline isn't needed if the next character is a closing curly bracket) but following this rule strictly will result in the least possible headache.
RTL is case-sensitive so `Table` and `table` are different things. If something isn't working the way you'd expect, make sure your casing is correct.

## Declarations

Every RTL file is made up of a series of **declarations**.
Declarations set top-level variables for use in the rest of the file.

### Default

`default` is one of the most important declarations in RTL.
When a file is loaded with a non-interactive tool, such as the command line, the `default` line is what is returned.
For example, [default.rtl](../examples/default.rtl) will produce the string `Hello World!` when processed, because that is the result of the `default` command.

### Table

The second most important declaration is `table`.
Most of an RTL file will be made up of `table`s.
[table.rtl](../examples/table.rtl) covers the basics of table usage, so let's break it down line-by-line.

```rtl
default pickATable[]
```

We know that the `default` command is the first thing that gets run, but what does `pickATable[]` do?
The `[]` at the end of the line is the **random-access** operator, which means that `pickATable` is going to be accessed randomly, but what is `pickATable`?

```rtl
table pickATable {
    :weightedTable[]
    :lookupTable[]
    :dictionaryTable[]
}
```

looking a the next block, we see our first `table` declaration.
`table` declarations consist of the word `table`, the name of our table (must start with a letter or underscore, has no spaces and is made up of letters, numbers and underscores) and a "block".
A block is the contents of a set of curly braces.
In this case, there are 3 lines in our block.
Each line starts with a colon, then has an object being randomly accessed.
When this table is randomly accessed, it will return one of these 3 lines at random.

```rtl
table weightedTable {
    type: weighted
    :`This item has a weight of 1`
    1:`This item also has a weight of 1`
    5:`This item has a weight of 5 and will likely be picked most often`
}
```

Next, we have our `weightedTable`.
The first line of this block is a **command entry**.
Command entries come in 3 kinds, which will be discussed shortly, but suffice to say, they `type` command sets how the table is randomly accessed; in this case, a `weighted` table.
As an aside, tables are `weighted` by default, so this line is technically unnecessary, but it's a good illustration.
Weighted tables use numeric weights to change how often a result is picked.
By default, lines have a weight of 1, but this can be chosen manually by putting a number in front of the `:`.
In this table, the last entry will come up 5&times; as much as the first or second.

You'll also notice the ``` ` ``` character at the beginning and end of the entry text.
This character (called a back-tick) is the **string delimiter** in RTL, so that `"` and `'` can be used in result strings without worry.

```rtl
table lookupTable {
    default: `You forgot to put 6 in the list, so we defaulted`
    roll:1d6
    type: lookup
    1-3:`You rolled low, too bad :(`
    4:`You rolled a 4`
    :`This line doesn't have a key, but you rolled a 5`
}
```

This is a `lookup` table.
Lookup tables are different from weighted tables by the nature of how they choose their result; rather than choosing based on weights, a lookup table uses the `roll` command to choose a number, then it checks each entry sequentially until it finds a match.
If it makes it all the way to the bottom, it uses the `default` commands value.
This can be useful when copying existing tables out of books that use multiple dice and would be tedious to calculate weights for.

The `roll` command for this table is `1d6`, which is standard dice notation for rolling a single six-sided die.
Those familiar with TTRPGs will recognize this, and it works exactly as you'd expect.
Any number of dice, any number of sides.
For special dice, such as FATE/Fudge, FF Star Wars, Exploding/rerollable dice, you'll need to define functions for them (described later).

Rather than using the number before the `:` as a weight, lookup tables use it as a value to be matched against (as seen on the 5th line).
Lookup tables also have the convenient **range operator** which is just two numbers separated by a `-`.
This allows you to have the same result for a span of numbers without adding up the odds of each.
In a lookup table, entries without a number (like the last one) take on a value equal to 1 more than the last entry (or the largest value in a range).

```rtl
table dictionaryTable {
    table animal {
        :`cat`
        :`dog`
        :`mouse`
    }
    let a = animal[]
    default: `The ${a} goes ${dictionaryTable[a]}`
    type: dictionary 
    `cat`:`meow`
    `dog`:`woof`
    `mouse`:`squeak`
}
```

Finally, we have our `dictionary` table.
This table type does not choose from itself randomly; instead, using the `[]` operator will just retrieve the default.
In this case, we take advantage of that fact to do something fancy.
Inside `dictionaryTable` there's another table!
This table can't be seen by other tables normally (since it lives inside the dictionary table), but this means that we can have a different `animal` table elsewhere without them overlapping.
Under the `animal` table declaration is a `let` declaration.
`let` defines a variable for future use, in this case, to store a result from `animal[]` into the `a` variable.
We then use this value to access `dictionaryTable`, retrieving the result with the same key.
the `${}` value inside the string is the secret sauce of RTL and is how you combine values and text.
Anything inside the curly braces is processed as variables, then added to the string, in this case, the animal name, followed by it's sound.
You can put anything you want in here, including dice rolls, math, and other more complicated functions.

### Import

The `import` declaration allows you to access tables, variables and functions from other files.
[import.rtl](../examples/import.rtl) shows the two ways files can be imported; with our without an identifier.

```rtl
import utils = `./utilities.rtl`
import `./explosives.rtl`
```

Importing files without an identifier is as if the file had been copied into this one, which means you can access tables by name directly.
This can be handy, but can also lead to name collisions.
To be maximally safe, you can import a file with a name.
This way, that whole file lives inside the name you gave it, and tables can be accessed with the `.` operator, as seen in the default declaration:

```rtl
default `${explosives[]} deals ${utils.explodingD6(3)} damage`
```

At this point, you should have everything you need to get started.

TODO: More tutorial