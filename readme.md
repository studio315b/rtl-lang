# RTL (Rollable Table Language)

A table rolling language for use in RPG prep, storytelling, or just providing that dash of inspiration.

## Syntax

The complete grammar of RTL is described in the [syntax file](./syntax.ebnf) in the W3C ENBF syntax.
You can read [RTL Basics](./docs/basics.md) for a more user-friendly guide.

## Library Usage

Library usage is dependant upon where and how you're using the tool, but for basic node.js applications where files are retrieved from the local disk, the following code will work:

```js
const { RTL, markdownFormatter } = require("rtl-lang");
const { cliErrorHandler, cliFileLoader } = require("rtl-lang/dist/cli");

const rtl = new RTL({
    errorHandler: cliErrorHandler,
    fileLoader: cliFileLoader,
    formatter: markdownFormatter
});

// to roll once
rtl.roll("./myTable.rtl").then((result) => {
    console.log(result);
});

// to create an object for rolling in the future
const myTable = await rtl.createTableSet("./myTable.rtl");

// generates a list of tables that can be rolled from this object
console.log(myTable.tables.join("\n"));

// rolls the default result
console.log(myTable.roll());

// rolls the named table
console.log(myTable.roll("myTable"));
```

## Command Line Usage

1. Have [node.js](https://nodejs.org/en) 18.17.0 or newer installed.
2. Install RTL globally with `npm install -g rtl-lang`.
   1. Ensure global npm binaries are in your path
3. Run `rtl [yourFile.rtl]` in bash/powershell/cmd/whatever.

### NAME

rtl - generate random result from table

### SYNOPSIS

`rtl [OPTION]... [TABLE] [COUNT=5]`

### DESCRIPTION

Generate `COUNT` random results from `TABLE` using format specified by option or table, defaulting to markdown.

`-a`
: Format results as asciidoc

`-h`
: Format results as html

`-m`
: Format results as markdown

`-p`
: Format results as plaintext

`-t`
: Format results as tiddlywiki

`-d`
: Provides debug information
