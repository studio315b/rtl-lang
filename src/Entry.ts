import { Token } from "./Token"
import { Expr } from "./Expr";
import { globalEnv } from "./environment/Environment";
import { evaluate } from "./Interpreter";

export abstract class Entry {
	readonly value: Expr[];
	readonly weight: number;
	constructor(value: Expr[], weight: number = 1) {
		this.value = value;
		this.weight = weight;
	}
	abstract matches(value: any): boolean;
}
export class CommandEntry extends Entry {
	readonly keyword: Token;
	constructor(keyword: Token, value: Expr[]) {
		super(value);
		this.keyword = keyword;
	}
	matches(_: any) {
		return false;
	}
}
export class CommonEntry extends Entry {
	constructor(value: Expr[]) {
		super(value);
	}

	matches(_: any) {
        return false;
    }
}
export class KeyedEntry extends Entry {
	readonly key: Expr;
	constructor(key: Expr, value: Expr[]) {
		super(value);
		this.key = evaluate(key, globalEnv);
	}
	matches(value: any) {
        return this.key == value;
    }
}
export class RangeEntry extends Entry {
	readonly min: number;
	readonly max: number;
	constructor(min: number, max: number, value: Expr[]) {
		super(value);
		this.min = min;
		this.max = max;
	}
    matches(value: any) {
        return this.min <= value && value <= this.max;
    }
}
export class WeightedEntry extends Entry {
	constructor(weight: number, value: Expr[]) {
		super(value, weight);
	}
	matches(value: any) {
        return this.weight == value;
    }
}