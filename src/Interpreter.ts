import { ArrayExpr, AssignExpr, BinaryExpr, CallExpr, EscapedExpr, Expr, GetExpr, GroupingExpr, IExprVisitor, IndexExpr, LambdaExpr, LiteralExpr, LogicalExpr, StringExpr, TernaryExpr, UnaryExpr, VariableExpr } from "./Expr";
import { BlockStmt, DefaultStmt, DefineStmt, EntryStmt, ExpressionStmt, FunctionStmt, IStmtVisitor, IfStmt, ImportStmt, ModuleStmt, ReturnStmt, SetStmt, Stmt, TableStmt, WhileStmt } from "./Stmt";
import { Token, TokenType } from "./Token";
import { Callable, Environment, InterpretedFunction, globalEnv, numberMethods, stringMethods } from "./environment";
import { Return, RuntimeError } from "./Types";
import { xDy } from "./utils/dice";

export function interpret(statements: Stmt[], baseEnv: Environment = globalEnv) {
    return new Interpreter(baseEnv).interpret(statements);
}

export function evaluate(expr: Expr, env: Environment) {
    return new Interpreter(env).evaluate(expr);
}

export function evaluateList(exprs: Expr[], env: Environment) {
    return new Interpreter(env).evaluateList(exprs);
}

export function execute(stmts: Stmt[], env: Environment) {
    return new Interpreter(env).execute(stmts);
}

class Interpreter implements IStmtVisitor<any>,  IExprVisitor<any> {

    private env: Environment;
    private imports: {key?: Token, value: string}[] = [];
    private inBlock: boolean = false;

    constructor(baseEnv: Environment) {
        this.env = new Environment("root", baseEnv);
    }

    interpret(stmts: Stmt[]) {
        this.execute(stmts);
        return { env: this.env, imports: this.imports };
    }

    execute(stmts: Stmt | Stmt[]) {
        if(stmts instanceof Stmt) stmts = [stmts];
        stmts.forEach(s => s.accept(this));
    }

    executeBlock(stmts: Stmt[], env: Environment) {
        const prevEnv = this.env;
        const prevInBlock = this.inBlock;
        this.env = env;
        this.inBlock = true;
        this.execute(stmts);
        this.env = prevEnv;
        this.inBlock = prevInBlock;
    }

    evaluate(expr: Expr): any {
        var result: any = expr;
        while(result instanceof Expr) {
            result = result.accept(this);
        }
        return result;
    }

    evaluateList(exprs: Expr[]) {
        var result: any = undefined;
        for(const expr of exprs) {
            result = this.evaluate(expr);
        }
        return result;
    }

    // IStmtVisitor
    visitBlockStmt(stmt: BlockStmt) {
        this.executeBlock(stmt.stmts, new Environment("block",this.env));
    }
    visitDefaultStmt(stmt: DefaultStmt) {
        this.env.defaultResult = stmt.exprs;
    }
    visitDefineStmt(stmt: DefineStmt) {
        this.env.define(stmt.name, stmt.value);
    }
    visitEntryStmt(stmt: EntryStmt) {
        this.env.addEntry(stmt.entry);
    }
    visitExpressionStmt(stmt: ExpressionStmt) {
        this.evaluate(stmt.expr);
    }
    visitFunctionStmt(stmt: FunctionStmt) {
        this.env.define(stmt.name, new InterpretedFunction(stmt.expr, this.env));
    }
    visitIfStmt(stmt: IfStmt) {
        if(isTruthy(this.evaluate(stmt.condition))) {
            this.execute(stmt.thenBranch);
            return;
        } 
        if(stmt.elseBranch){
            this.execute(stmt.elseBranch);
        }
    }
    visitImportStmt(stmt: ImportStmt) {
        if(this.inBlock) {
            throw new RuntimeError(stmt.keyword, "Cannot import from within a block.")
        }
        this.imports.push({key: stmt.identifier, value: this.evaluate(stmt.path)});
    }

    visitModuleStmt(stmt: ModuleStmt) {
        var env = new Environment(stmt.name.lexeme, this.env);
        this.executeBlock(stmt.declarations, env);
        this.env.define(stmt.name, env);
    }

    visitReturnStmt(stmt: ReturnStmt) {
        let value: any;
        if(stmt.expr) value = this.evaluate(stmt.expr);
        throw new Return(value);
    }
    visitSetStmt(stmt: SetStmt) {
        this.env.define(stmt.name, this.evaluate(stmt.value));
    }
    visitTableStmt(stmt: TableStmt) {
        const env = new Environment(stmt.name.lexeme, this.env, true);
        this.executeBlock(stmt.declarations, env);
        stmt.entries.forEach(e => env.addEntry(e));
        this.env.define(stmt.name, env);
    }
    visitWhileStmt(stmt: WhileStmt) {
        while(isTruthy(this.evaluate(stmt.condition))) {
            this.execute(stmt.body);
        }
    }
 
    // IExprVisitor

    visitArrayExpr(expr: ArrayExpr) {
        const elements = expr.values.map(this.evaluate.bind(this));
        return elements;
    }

    visitAssignExpr(expr: AssignExpr) {
        const value = this.evaluate(expr.value);
        if(expr.key instanceof VariableExpr) {
            this.env.assign(expr.key.name, value);
        }
        if(expr.key instanceof IndexExpr) {
            const arr = expr.key;
            const target = this.evaluate(arr.object);
            if(!arr.key) {
                throw new RuntimeError(arr.square, "Cannot set empty accessor.")
            }
            if(Array.isArray(target)) {
                target[this.evaluate(arr.key)] = this.evaluate(expr.value);
            }
            throw new RuntimeError(arr.square, "Cannot assign to non-array.")
        }
    }
    
    visitBinaryExpr(expr: BinaryExpr) {
        const left = this.evaluate(expr.left);
        const right = this.evaluate(expr.right);

        switch(expr.op.type) {
            case TokenType.PLUS:
                if(isNumber(left) && isNumber(right)) return left+right;
                if(isArray(left) && isArray(right)) return [...left, ...right];
                return left.toString() + right.toString();
            case TokenType.MINUS:
                checkNumberOperands(expr.op,left,right);
                return left-right;
            case TokenType.STAR:
                checkNumberOperands(expr.op,left,right);
                return left * right;
            case TokenType.SLASH:
                checkNumberOperands(expr.op,left,right);
                return left * right;
            case TokenType.D:
                checkNumberOperands(expr.op,left,right);
                return xDy(left,right);
            case TokenType.GREATER:
                checkNumberOperands(expr.op,left,right);
                return left > right;
            case TokenType.GREATER_EQUALS:
                checkNumberOperands(expr.op,left,right);
                return left >= right;
            case TokenType.LESS:
                checkNumberOperands(expr.op,left,right);
                return left < right;
            case TokenType.LESS_EQUALS:
                checkNumberOperands(expr.op,left,right);
                return left <= right;
            case TokenType.EQUALS_EQUALS:
                return left === right;
            case TokenType.BANG_EQUALS:
                return left !== right;
        }

    }

    visitCallExpr(expr: CallExpr) {
        const callee = this.evaluate(expr.callee);
        const args: any[] = [];
        for(const arg of expr.args) {
            args.push(this.evaluate(arg));
        }
        if(callee instanceof Callable)
            return callee.call(args, this.env);
        throw new RuntimeError(expr.paren, "Called value is not a function.");
    }

    visitEscapedExpr(expr: EscapedExpr) {
        switch(expr.value.lexeme) {
            case "\\n":
                return "\n";
            case "\\r":
                return "\r";
            case "\\t":
                return "\t";
            case "\\_":
                return this.env.formatter.space();
            default:
                return expr.value.lexeme[1];
        }
    }

    visitGetExpr(expr: GetExpr) {
        let target = this.evaluate(expr.object);
        if(target instanceof Environment) {
            return target.get(expr.name);
        }
        if(typeof(target) == "number") {
            if(numberMethods.has(expr.name)) {
                return numberMethods.get(expr.name, target);
            }
            target = target.toString();
        }
        if(typeof(target) == "string") {
            return stringMethods.get(expr.name, target);
        }
        throw new RuntimeError(expr.name, "Target does not have properties.")
    }

    visitGroupingExpr(expr: GroupingExpr) {
        return this.evaluate(expr.inner);
    }

    visitIndexExpr(expr: IndexExpr) {
        const target = this.evaluate(expr.object);
        let key = expr.key ? this.evaluate(expr.key) : undefined;
        if(target instanceof Environment) {
            return target.roll(key);
        }
        if(Array.isArray(target) && key == undefined) {
            return target[xDy(1, target.length, -1)];
        }
        if(!expr.key) throw new RuntimeError(expr.square, "Empty accessor only available for tables and arrays.")
        if(typeof key != "number") key = parseInt(key)
        if(Number.isNaN(key)) throw new RuntimeError(expr.square, "Array accessor must be a number");
        if(Array.isArray(target)) {
            return target[key];
        }
        if(typeof(target) == "string") {
            return target[key];
        }
        throw new RuntimeError(expr.square, "Cannot use index accessor on object.");
    }

    visitLambdaExpr(expr: LambdaExpr) {
        return new InterpretedFunction(expr, this.env)
    }

    visitLiteralExpr(expr: LiteralExpr) {
        return expr.value;
    }

    visitLogicalExpr(expr: LogicalExpr) {
        const left = this.evaluate(expr.left)
        if(expr.op.type == TokenType.OR) {
            if(isTruthy(left)) return left;
        } else {
            if(!isTruthy(left)) return left;
        }

        return this.evaluate(expr.right);
    }

    visitStringExpr(expr: StringExpr) {
        let parts: any[] = []
        for(const part of expr.parts) {
            parts.push(this.evaluate(part));
        }
        return parts.join("");
    }

    visitTernaryExpr(expr: TernaryExpr) {
        return isTruthy(this.evaluate(expr.cond)) ? this.evaluate(expr.thenExpr) : this.evaluate(expr.elseExpr);
    }

    visitUnaryExpr(expr: UnaryExpr) {
        const right = this.evaluate(expr.right);
        switch(expr.op.type) {
            case TokenType.BANG:
                return !isTruthy(right);
            case TokenType.MINUS:
                checkNumberOperand(expr.op, right);
                return -right;
        }
    }

    visitVariableExpr(expr: VariableExpr) {
        return this.env.get(expr.name);
    }
}

function isNumber(v:any) {
    return typeof(v) == "number"
}

function isArray(v: any) {
    return Array.isArray(v);
}

function isTruthy(v: any) {
    switch(typeof(v)) {
        case "boolean":
            return v;
        case "string":
            return v.trim().length != 0;
        case "number":
            return v != 0;
        default:
            return !!v;
    }
}

function checkNumberOperand(op: Token, operand: any) {
    if(!isNumber(operand)) {
        throw new RuntimeError(op, "Operand must be a number.");
    }
}

function checkNumberOperands(op: Token, left: any, right: any) {
    if(!isNumber(left) || !isNumber(right)) {
        throw new RuntimeError(op, "Operands must be numbers.");
    }
}