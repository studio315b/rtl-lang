import { dirname, isAbsolute, resolve } from "path";
import { Token, TokenType } from "../Token";
import { ErrorHandler, FileLoader } from "../Types";
import { readFile } from "fs/promises";

function report(file: string, line: number, where: string, message: string) {
    cliErrorHandler.hadError = true;
    console.error(`${file}:${line}\t Error${where}: ${message}`);
}

export const cliErrorHandler: ErrorHandler = {
    hadError: false,
    tokenError(token: Token, message: string) {
        report(token.file, token.line, token.type == TokenType.EOF ? " at end" : ` at '${token.lexeme}'`, message);
    },
    fileError(file: string, line: number, message: string) {
        report(file, line, "", message);
    },
    error(message: string) {
        console.error(message);
    },
}

export const cliFileLoader: FileLoader = {
    absolutePath(path: string, relativeTo?: string) {
        if(isAbsolute(path)) {
            return path;
        }
        if(relativeTo) {
            return resolve(dirname(relativeTo), path);
        }
        return resolve(path)
        
    },
    async loadFile(path: string) {
        let file = await readFile(path);
        return file.toString();
    }

}