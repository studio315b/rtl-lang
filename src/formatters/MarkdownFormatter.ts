import { Formatter } from "../Types";

export const markdownFormatter: Formatter = {
    bold(value: string): string {
        return `**${value}**`;
    },
    italic(value: string): string {
        return `*${value}*`;
    },
    space(): string {
        return " ";
    }
}

