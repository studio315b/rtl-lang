import { evaluateList } from "./Interpreter";
import { ErrorHandler, RuntimeError } from "./Types";
import { Environment } from "./environment";

export class TableSet {
    private readonly env: Environment
    private readonly errorHandler: ErrorHandler
    constructor(env: Environment, errorHandler: ErrorHandler) {
        this.env = env;
        this.errorHandler = errorHandler;
    }

    get tables(): string[] {
        return ["default", ...this.env.tables];
    }

    roll(name: string = "default", value: any = undefined): string {
        try {
            // Clone to prevent side effects on future rolls
            const env = this.env.clone();
            if (name == "default" && env.defaultResult) {
                let v = evaluateList(env.defaultResult, env);
                return v;
            }
            const table = env.getLocalOnly(name) as Environment;
            if (!table) return `Table '${name}' not found.`
            return table.roll(value);
        } catch (e) {
            if (e instanceof RuntimeError) {
                this.errorHandler.tokenError(e.token, e.message);
                return "";
            }
            if(e instanceof Error) {
                this.errorHandler.error(e.message)
                return "";
            }
            this.errorHandler.error(e as any);
            return "";
           
        }
    }
}