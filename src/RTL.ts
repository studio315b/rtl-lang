import { evaluateList, interpret } from "./Interpreter";
import { Parser } from "./Parser";
import { Scanner } from "./Scanner";
import { Stmt } from "./Stmt";
import { Token } from "./Token";
import { Environment, globalEnv } from "./environment";
import { ErrorHandler, FileLoader, Formatter, RuntimeError } from "./Types";
import { TableSet } from "./TableSet";

type tableFunction = (result?: string | number) => string;

type RTLConfig = {
    errorHandler: ErrorHandler,
    fileLoader: FileLoader,
    formatter?: Formatter;
}

export class RTL {
    readonly errorHandler: ErrorHandler;
    readonly fileLoader: FileLoader;
    readonly scanner: Scanner;
    readonly parser: Parser;
    readonly globalEnv: Environment;
    readonly statementsCache: Record<string, Stmt[]> = {};

    constructor(config: RTLConfig) {
        this.errorHandler = config.errorHandler;
        this.fileLoader = config.fileLoader;
        this.scanner = new Scanner(config.errorHandler);
        this.parser = new Parser(config.errorHandler);
        this.globalEnv = globalEnv.clone();
        if(config.formatter) this.globalEnv.formatter = config.formatter;
    }

    async roll(path: string, reload: boolean = false): Promise<string> {
        this.errorHandler.hadError = false;
        let absolute = this.fileLoader.absolutePath(path);
        const env = await this.createFileEnv(absolute, reload);
        if (this.errorHandler.hadError) {
            this.errorHandler.error("Errors in file");
            return "";
        }
        if (env.defaultResult.length == 0) {
            this.errorHandler.error("No default table found")
        }
        try {
            return evaluateList(env.defaultResult, env) as string ?? "";
        } catch (e) {
            if (e instanceof RuntimeError) {
                this.errorHandler.tokenError(e.token, e.message);
            }
        }
        return "";

    }

    async createTableSet(path: string, reload: boolean = false) {
        this.errorHandler.hadError = false;
        let absolute = this.fileLoader.absolutePath(path);
        const env = await this.createFileEnv(absolute, reload);
        if (this.errorHandler.hadError) {
            this.errorHandler.error("Errors in file");
            return undefined;
        }
        return new TableSet(env, this.errorHandler);
    }

    private async createFileEnv(path: string, reload: boolean) {
        const resolveImports = async (env: Environment, imports: { key?: Token, value: string }[], fromFile: string) => {
            for (const i of imports) {
                const path = this.fileLoader.absolutePath(i.value, fromFile)
                const value = await this.createFileEnv(path, reload);
                if (i.key) env.define(i.key, value);
                else value.copyInto(env);
            }
        }
        if(reload) delete this.statementsCache[path];
        let statements = this.statementsCache[path];
        if (!statements) {
            const source = await this.fileLoader.loadFile(path);
            const tokens = this.scanner.scan(source, path);
            statements = this.parser.parse(tokens);
            if(!this.errorHandler.hadError) this.statementsCache[path] = statements;
        }
        const { env, imports } = interpret(statements, this.globalEnv);
        await resolveImports(env, imports, path);
        return env;
    }
}