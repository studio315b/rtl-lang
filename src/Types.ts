import { Token } from "./Token";

export type ErrorHandler = {
    hadError: boolean;
    fileError(file: string, line: number, message: string): void;
    tokenError(token: Token, message: string): void;
    error(message: string): void;
}

export type FileLoader = {
    absolutePath(path: string, relativeTo?: string): string;
    loadFile(path: string): Promise<string>;
}

export type Formatter = {
    bold(value: string): string;
    italic(value: string): string;
    space(): string;
}

export class RuntimeError extends Error {
    readonly token: Token;

    constructor(token: Token, message: string) {
        super(message);
        this.token = token;
    }
}

export class TypeMatchError extends Error {
    constructor(expected: string, actual: string) {
        super(`Type match error: Expected '${expected}' received '${actual}'.`)
    }
}

export class Return extends Error {
    readonly value: any;
    constructor(value: any) {
        super()
        this.value = value;
    }
}