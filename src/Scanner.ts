import { Token, TokenType } from "./Token";
import { ErrorHandler } from "./Types";

export class Scanner {
    errorHandler: ErrorHandler;

    constructor(errorHandler: ErrorHandler) {
        this.errorHandler = errorHandler;
    }

    scan(source: string, file: string): Token[] {
        return new ScannerContext(this.errorHandler, source, file).scan();
    }
}

const keywords: Record<string, TokenType> = {
    "default": TokenType.DEFAULT,
    "define": TokenType.DEFINE,
    "dictionary": TokenType.DICTIONARY,
    "else": TokenType.ELSE,
    "export": TokenType.EXPORT,
    "false": TokenType.FALSE,
    "func": TokenType.FUNC,
    "if": TokenType.IF,
    "import": TokenType.IMPORT,
    "let": TokenType.LET,
    "lookup": TokenType.LOOKUP,
    "module": TokenType.MODULE,
    "return": TokenType.RETURN,
    "roll": TokenType.ROLL,
    "table": TokenType.TABLE,
    "true": TokenType.TRUE,
    "type": TokenType.TYPE,
    "weighted": TokenType.WEIGHTED,
    "while": TokenType.WHILE,
}

class ScannerContext {
    readonly errorHandler: ErrorHandler;
    readonly source: string;
    readonly file: string;
    readonly tokens: Token[] = [];
    line: number = 1;
    current: number = 0;
    start: number = 0;

    get isAtEnd() {
        return this.current >= this.source.length;
    }
    get lexeme() {
        return this.source.slice(this.start, this.current);
    }

    constructor(errorHandler: ErrorHandler, source: string, file: string) {
        this.errorHandler = errorHandler;
        this.source = source
        this.file = file;
    }

    scan() {
        while(!this.isAtEnd) {
            this.start = this.current;
            this.scanToken()
        }
        this.tokens.push(new Token(TokenType.EOF, "", null, this.file, this.line));
        return this.tokens;
    }

    scanToken() {
        const c = this.advance();
        switch(c) {
            case "(": this.addToken(TokenType.OPEN_PAREN); break;
            case ")": this.addToken(TokenType.CLOSE_PAREN); break;
            case "{": this.addToken(TokenType.OPEN_CURLY); break;
            case "}": this.addToken(TokenType.CLOSE_CURLY); break;
            case "[": this.addToken(TokenType.OPEN_SQUARE); break;
            case "]": this.addToken(TokenType.CLOSE_SQUARE); break;
            case ":": this.addToken(TokenType.COLON); break;
            case "-": this.addToken(TokenType.MINUS); break;
            case "+": this.addToken(TokenType.PLUS); break;
            case "*": this.addToken(TokenType.STAR); break;
            case ".": this.addToken(TokenType.DOT); break;
            case "&": this.addToken(TokenType.AND); break;
            case "|": this.addToken(TokenType.OR); break;
            case ",": this.addToken(TokenType.COMMA); break;
            case "?": this.addToken(TokenType.QUESTION); break;
            case "/":
                if(this.match("/")) {
                    while(this.peek() != "\n" && !this.isAtEnd) this.advance();
                    return;
                } else {
                    this.addToken(TokenType.SLASH);
                }
            case "!":
                this.addToken(this.match("=") ? TokenType.BANG_EQUALS : TokenType.BANG);
                break;
            case "=":
                this.addToken(this.match("=") ? TokenType.EQUALS_EQUALS : TokenType.EQUALS);
                break;
            case ">":
                this.addToken(this.match("=") ? TokenType.GREATER_EQUALS : TokenType.GREATER);
                break;
            case "<":
                this.addToken(this.match("=") ? TokenType.LESS_EQUALS : TokenType.LESS);
                break;
            case " ":
            case "\r":
            case "\t":
                break;
            case "\n":
                this.addToken(TokenType.NL);
                this.line++;
                break;
            case "`":
                this.string();
                break;
            default:
                if(this.isDigit(c)) {
                    this.number();
                } else if(this.isAlpha(c)) {
                    this.identifier();
                } else {
                    this.error(`Unexpected character "${c}".`);
                }
                break;
        }
    }

    number() {
        while(this.isDigit(this.peek())) this.advance();

        if(this.peek() == "." && this.isDigit(this.peekNext())) {
            this.advance();
            while(this.isDigit(this.peek())) this.advance();
        }
        this.addToken(TokenType.NUMBER, Number.parseFloat(this.lexeme));
    }
    
    identifier() {
        while(this.isAlphanumeric(this.peek())) this.advance();
        // special check for d[number]
        if(/^d\d*$/.test(this.lexeme)) {
            this.current = this.start + 1;
            this.addToken(TokenType.D);
            return;
        }
        this.addToken(keywords[this.lexeme] ?? TokenType.IDENTIFIER);
    }

    string() {
        this.addToken(TokenType.TICK);
        this.start = this.current;
        while(this.peek() != "`" && !this.isAtEnd) {
            let c = this.advance();
            switch (c) {
                case "$":
                    if(this.peek() == "{") {
                        this.addRaw();
                        this.current += 2;
                        this.addToken(TokenType.OPEN_CURLY);
                        this.inline();
                    }
                    break;
                case "\\":
                    this.addRaw();
                    this.current += 2;
                    this.addToken(TokenType.ESCAPE);
                    this.start = this.current;
                    break;
                default: break;
            }
        }
        if(this.isAtEnd) {
            this.error("Unterminated string.");
        }
        if(this.start != this.current) {
            this.addToken(TokenType.RAW_STRING);
            this.start = this.current
        }
        // terminating backtick
        this.advance();
        this.addToken(TokenType.TICK);
    }

    inline() {
        while(this.tokens[this.tokens.length - 1].type != TokenType.CLOSE_CURLY && !this.isAtEnd) {
            this.start = this.current;
            this.scanToken();
        }
        this.start = this.current;
    }

    addRaw() {
        this.current--;
        if(this.start != this.current) {
            this.addToken(TokenType.RAW_STRING);
            this.start = this.current
        }
    }

    advance() {
        return this.source[this.current++];
    }

    peek() {
        if(this.isAtEnd) return "\0";
        return this.source[this.current]
    }

    peekNext(){
        if(this.current + 1 >= this.source.length) return '\0';
        return this.source[this.current+1];
    }

    match(...options: string[]) {
        if(this.isAtEnd) return false;
        if(!options.includes(this.source[this.current])) return false;

        this.current++;
        return true;
    }

    error(message: string) {
        this.errorHandler.fileError(this.file, this.line, message)
    }

    addToken(type: TokenType, literal: any = undefined) {
        this.tokens.push(new Token(type, this.lexeme, literal, this.file, this.line));
    }

    isDigit(c: string) {
        return "0" <= c && c <= "9";
    }

    isAlpha(c: string) {
        return c == "_"
            || ("a" <= c && c <= "z")
            || ("A" <= c && c <= "Z");
    }

    isAlphanumeric(c: string) {
        return this.isDigit(c) || this.isAlpha(c);
    }
}