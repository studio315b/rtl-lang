import { RTL } from "./RTL";
import { Formatter } from "./Types";
import { cliErrorHandler, cliFileLoader } from "./cli";
import { markdownFormatter, plaintextFormatter } from "./formatters";

const DEFAULT_FORMATTER = plaintextFormatter;
const DEFAULT_COUNT = 5;

function error(message: string) {
    console.error(message);
    process.exit(-1);
}

let args = process.argv.slice(2);

let formatter: Formatter | undefined;
let file: string | undefined;
let count: number | undefined;
let debug = false;

for (const arg of args) {
    switch (arg) {
        case "-d":
            debug = true;
            break;
        case "-a":
            if (formatter != undefined) error("Format defined multiple times");
            error("Asciidoc formatter not implemented.");
            break;
        case "-h":
            if (formatter != undefined) error("Format defined multiple times");
            error("HTML formatter not implemented.");
            break;
        case "-m":
            if (formatter != undefined) error("Format defined multiple times");
            formatter = markdownFormatter;
            break;
        case "-p":
            if (formatter != undefined) error("Format defined multiple times");
            formatter = plaintextFormatter;
            break;
        case "-t":
            if (formatter != undefined) error("Format defined multiple times");
            error("TiddlyWiki formatter not implemented.")
            break;
        default:
            if (file == undefined) {
                file = arg;
            } else if (count == undefined) {
                count = parseInt(arg);
                if (Number.isNaN(count)) error(`Unexpected argument '${arg}'`);
            }
            break;
    }
}
if (formatter == undefined) {
    formatter = DEFAULT_FORMATTER;
}
if (count == undefined) {
    count = DEFAULT_COUNT;
}
if (file == undefined) {
    error("No file selected");
}

const startTime = Date.now();
const rtl = new RTL({
    errorHandler: cliErrorHandler,
    fileLoader: cliFileLoader,
    formatter
});

(async () => rtl.createTableSet(file as string))()
    .then((set) => {
        if (!set) {
            console.error("Failed to create table set")
            return;
        }
        for (var i = 0; i < count!; i++) {
            console.log(set.roll());
        }
        if (debug) {
            console.log(`Time Elapsed: ${Date.now() - startTime}ms`);
        }
        process.exit(0);
    })