import { CommandEntry, CommonEntry, Entry, KeyedEntry, RangeEntry, WeightedEntry } from "./Entry";
import { ArrayExpr, AssignExpr, BinaryExpr, CallExpr, EscapedExpr, Expr, GetExpr, GroupingExpr, IndexExpr, LambdaExpr, LiteralExpr, StringExpr, TernaryExpr, UnaryExpr, VariableExpr } from "./Expr";
import { BlockStmt, DefaultStmt, DefineStmt, ExpressionStmt, FunctionStmt, IfStmt, ImportStmt, ModuleStmt, ReturnStmt, SetStmt, Stmt, TableStmt, WhileStmt } from "./Stmt";
import { Token, TokenType } from "./Token";
import { ErrorHandler, RuntimeError } from "./Types";

export class Parser {
    errorHandler: ErrorHandler
    constructor(errorHandler: ErrorHandler) {
        this.errorHandler = errorHandler;
    }

    parse(tokens: Token[]): Stmt[] {
        return new ParserContext(this.errorHandler, tokens).parse();
    }
}

class ParserContext {
    errorHandler: ErrorHandler
    tokens: Token[];
    current: number = 0;
    get isAtEnd() {
        return this.tokens[this.current].type == TokenType.EOF;
    };

    constructor(errorHandler: ErrorHandler, tokens: Token[]) {
        this.errorHandler = errorHandler;
        this.tokens = tokens;
    }

    parse() {
        const declarations: Stmt[] = [];
        while (!this.isAtEnd) {
            const next = this.declaration();
            if (next != undefined) {
                declarations.push(next);
            }
        }
        return declarations;
    }

    declaration() {
        try {
            if (this.match(TokenType.NL)) return undefined;
            if (this.match(TokenType.DEFAULT)) return this.defaultStmt();
            if (this.match(TokenType.DEFINE)) return this.defineStmt();
            if (this.match(TokenType.FUNC)) return this.functionStmt();
            if (this.match(TokenType.IMPORT)) return this.importDecl();
            if (this.match(TokenType.MODULE)) return this.moduleDecl();
            if (this.match(TokenType.LET)) return this.setStmt();
            if (this.match(TokenType.TABLE)) return this.tableDecl();

            throw new RuntimeError(this.peek(), "Unexpected non-declaration.");
        } catch (e) {
            if (e instanceof RuntimeError) {
                this.errorHandler.tokenError(e.token, e.message);
                this.synchronize();
                return undefined;
            }
            throw e;
        }
    }


    importDecl() {
        let keyword = this.previous();
        let name: Token | undefined;
        if (this.match(TokenType.IDENTIFIER)) {
            name = this.previous();
            this.consume(TokenType.EQUALS, "Expect '=' after import alias.")
        }
        this.consume(TokenType.TICK, "Expect file reference in import.")
        const path = this.string();
        this.consumeNL();
        return new ImportStmt(keyword, path, name);
    }

    moduleDecl() {
        const name = this.consume(TokenType.IDENTIFIER, "Expect module name.");
        this.consume(TokenType.OPEN_CURLY, "Expect '{' before module body.");
        const declarations: Stmt[] = [];
        while (!this.check(TokenType.CLOSE_CURLY) && !this.isAtEnd) {
            const next = this.declaration();
            if (next != undefined) {
                declarations.push(next);
            }
        }
        this.consume(TokenType.CLOSE_CURLY, "Expect '}' after block.")
        this.consumeNL();
        return new ModuleStmt(name, declarations);
    }

    tableDecl() {
        const name = this.consume(TokenType.IDENTIFIER, "Expect table name.");
        this.consume(TokenType.OPEN_CURLY, "Expect '{' before table body.");
        const declarations: Stmt[] = [];
        const entries: Entry[] = [];
        while (!this.check(TokenType.CLOSE_CURLY) && !this.isAtEnd) {
            if (this.match(TokenType.NL)) continue;
            const entry = this.entry();
            if (entry) {
                entries.push(entry);
                continue;
            }
            const decl = this.declaration();
            if (decl != undefined) {
                declarations.push(decl);
            }
        }
        this.consume(TokenType.CLOSE_CURLY, "Expect '}' after block.")
        this.consumeNL();
        return new TableStmt(name, declarations, entries);

    }

    entry() {
        if (this.match(TokenType.DEFAULT, TokenType.ROLL)) return this.commandEntry();
        if (this.match(TokenType.TYPE)) return this.typeEntry();
        if (this.match(TokenType.COLON)) return this.commonEntry();
        if (this.match(TokenType.TICK)) return this.keyedEntry();
        if (this.match(TokenType.NUMBER)) return this.numberedEntry();
        return undefined;
    }

    commandEntry() {
        const keyword = this.previous();
        this.consume(TokenType.COLON, "Expect ':' between command and value in entry.");
        const expr = this.argsList();
        return new CommandEntry(keyword, expr);
    }

    typeEntry() {
        this.consume(TokenType.COLON, "Expect ':' between command and value in entry.");
        if (!this.match(TokenType.WEIGHTED, TokenType.LOOKUP, TokenType.DICTIONARY)) {
            throw new RuntimeError(this.peek(), "Expect 'weighted' 'lookup' or 'dictionary' for type.");
        }
        const type = this.previous();
        this.consume(TokenType.NL, "Expect newline after entry.");
        return new CommandEntry(type, []);
    }

    commonEntry() {
        return new CommonEntry(this.argsList());
    }

    keyedEntry() {
        const token = this.previous();
        const key = this.string();
        if(!key.parts.every(v => (v instanceof LiteralExpr || v instanceof EscapedExpr))) {
            throw new RuntimeError(token, "Key expressions must be static.");
        }
        this.consume(TokenType.COLON, "Expect ':' between key and value in entry.");
        return new KeyedEntry(key, this.argsList());
    }

    numberedEntry() {
        const min = this.previous();
        let max: Token | undefined;
        if (this.match(TokenType.MINUS)) {
            max = this.consume(TokenType.NUMBER, "Expect number after '-' in range entry.")
        }
        this.consume(TokenType.COLON, "Expect ':' between key and value in entry.");
        const value = this.argsList();
        if (max) return new RangeEntry(min.literal, max.literal, value);
        return new WeightedEntry(min.literal, value)
    }

    statement(): Stmt {
        if (this.match(TokenType.OPEN_CURLY)) return new BlockStmt(this.block());
        if (this.match(TokenType.DEFINE)) return this.defineStmt();
        if (this.match(TokenType.FUNC)) return this.functionStmt();
        if (this.match(TokenType.IF)) return this.ifStmt();
        if (this.match(TokenType.RETURN)) return this.returnStmt();
        if (this.match(TokenType.LET)) return this.setStmt();
        if (this.match(TokenType.WHILE)) return this.whileStmt();
        return this.expressionStmt();
    }

    block() {
        const stmts: Stmt[] = [];
        while (!this.check(TokenType.CLOSE_CURLY) && !this.isAtEnd) {
            if (this.match(TokenType.NL)) continue;
            stmts.push(this.statement());
        }
        this.consume(TokenType.CLOSE_CURLY, "Expect '}' after block.")
        return stmts;
    }

    defaultStmt() {
        const value = this.argsList();
        this.consumeNL();
        return new DefaultStmt(value);

    }

    defineStmt() {
        const name = this.consume(TokenType.IDENTIFIER, "Expected variable name.");
        this.consume(TokenType.EQUALS, "Expect initializer for variable declaration.");
        const value = this.expression();
        this.consumeNL();
        return new DefineStmt(name, value);
    }

    expressionStmt() {
        const expr = this.expression();
        this.consumeNL();
        return new ExpressionStmt(expr);
    }

    functionStmt() {
        const name = this.consume(TokenType.IDENTIFIER, "Expected function name.");
        let lambda = this.lambda();
        this.consumeNL();
        return new FunctionStmt(name, lambda);
    }

    ifStmt() {
        this.consume(TokenType.OPEN_PAREN, "Expect '(' after 'if'.");
        const cond = this.expression();
        this.consume(TokenType.CLOSE_PAREN, "Expect ')' after if condition.");
        const thenBranch = this.statement();
        let elseBranch: Stmt | undefined = undefined;
        if (this.match(TokenType.ELSE)) {
            elseBranch = this.statement();
        }
        return new IfStmt(cond, thenBranch, elseBranch);
    }

    returnStmt() {
        const token = this.previous();
        const expr = this.expression();
        this.consumeNL();
        return new ReturnStmt(token, expr);
    }

    setStmt() {
        const name = this.consume(TokenType.IDENTIFIER, "Expect variable name.")
        this.consume(TokenType.EQUALS, "Expect initializer for variable declaration.");
        const value = this.expression();
        
        return new SetStmt(name, value);
    }

    whileStmt() {
        this.consume(TokenType.OPEN_PAREN, "Expect '(' after 'while'.");
        const cond = this.expression();
        this.consume(TokenType.CLOSE_PAREN, "Expect ')' after while condition.");
        const body = this.statement();
        return new WhileStmt(cond, body);
    }

    expression(): Expr {
        return this.assignment();
    }

    assignment(): Expr {
        const expr = this.ternary();
        if (this.match(TokenType.EQUALS)) {
            let equals = this.previous();
            const value = this.assignment();
            if (expr instanceof VariableExpr || expr instanceof IndexExpr) {
                return new AssignExpr(expr, value);
            }
            this.errorHandler.tokenError(equals, "Invalid assignment target.")
        }
        return expr;
    }

    ternary() {
        let expr = this.logicOr();
        if (this.match(TokenType.QUESTION)) {
            const thenExpr = this.logicOr();
            this.consume(TokenType.COLON, "Expect ':' in ternary expression.");
            const elseExpr = this.logicOr();
            expr = new TernaryExpr(expr, thenExpr, elseExpr);
        }
        return expr;
    }

    logicOr() {
        return this.binaryGenerator(this.logicAnd, TokenType.OR);
    }

    logicAnd() {
        return this.binaryGenerator(this.equality, TokenType.AND);
    }

    equality() {
        return this.binaryGenerator(this.comparison, TokenType.BANG_EQUALS, TokenType.EQUALS_EQUALS);
    }

    comparison() {
        return this.binaryGenerator(this.term, TokenType.GREATER, TokenType.GREATER_EQUALS, TokenType.LESS, TokenType.LESS_EQUALS);
    }

    term() {
        return this.binaryGenerator(this.factor, TokenType.PLUS, TokenType.MINUS);
    }

    factor() {
        return this.binaryGenerator(this.unary, TokenType.STAR, TokenType.SLASH);
    }

    unary(): Expr {
        if (this.match(TokenType.BANG, TokenType.MINUS)) {
            const op = this.previous();
            const right = this.unary();
            return new UnaryExpr(op, right);
        }
        return this.call();
    }

    call() {
        let expr = this.roll();
        while (true) {
            if (this.match(TokenType.OPEN_PAREN)) {
                let args: Expr[] = [];
                if (!this.check(TokenType.CLOSE_PAREN)) {
                    do {
                        args.push(this.expression());
                    } while (this.match(TokenType.COMMA));
                }
                const paren = this.consume(TokenType.CLOSE_PAREN, "Expect ')' after arguments.");
                expr = new CallExpr(expr, paren, args);
                continue;
            }
            if (this.match(TokenType.OPEN_SQUARE)) {
                let square =this.previous();
                let key: Expr | undefined = undefined;
                if (!this.check(TokenType.CLOSE_SQUARE)) {
                    key = this.expression();
                }
                this.consume(TokenType.CLOSE_SQUARE, "Expect ']' after accessor.");
                expr = new IndexExpr(expr, square, key);
                continue;
            }
            if (this.match(TokenType.DOT)) {
                const name = this.consume(TokenType.IDENTIFIER, "Expect property name after '.'.");
                expr = new GetExpr(expr, name);
                continue;
            }
            break;
        }
        return expr;
    }

    roll() {
        return this.binaryGenerator(this.primary, TokenType.D);
    }

    primary() {
        if (this.match(TokenType.FALSE)) return new LiteralExpr(false);
        if (this.match(TokenType.TRUE)) return new LiteralExpr(true);
        if (this.match(TokenType.NUMBER)) return new LiteralExpr(this.previous().literal);
        if (this.match(TokenType.IDENTIFIER)) return new VariableExpr(this.previous());
        if (this.match(TokenType.TICK)) return this.string();
        if (this.match(TokenType.FUNC)) return this.lambda();

        if (this.match(TokenType.OPEN_SQUARE)) {
            let args: Expr[] = [];
            if (!this.check(TokenType.CLOSE_SQUARE) && !this.isAtEnd) {
                do {
                    args.push(this.expression());
                } while (this.match(TokenType.COMMA));
            }
            this.consume(TokenType.CLOSE_SQUARE, "Unterminated array.");
            return new ArrayExpr(args);
        }

        if (this.match(TokenType.OPEN_PAREN)) {
            const expr = this.expression();
            this.consume(TokenType.CLOSE_PAREN, "Expect ')' after expression.")
            return new GroupingExpr(expr);
        }
        throw new RuntimeError(this.peek(), "No expression found.");
    }

    string() {
        const parts: Expr[] = [];
        while (!this.check(TokenType.TICK) && !this.isAtEnd) {
            if (this.match(TokenType.RAW_STRING)) parts.push(new LiteralExpr(this.previous().lexeme));
            if (this.match(TokenType.ESCAPE)) parts.push(new EscapedExpr(this.previous()));
            if (this.match(TokenType.OPEN_CURLY)) {
                parts.push(this.expression());
                this.consume(TokenType.CLOSE_CURLY, "Unterminated inline expression.");
            }
        }
        this.consume(TokenType.TICK, "Unterminated string.");
        return new StringExpr(parts);
    }

    lambda() {
        this.consume(TokenType.OPEN_PAREN, "Expect parameter list in func.");
        const params: Token[] = []
        if (!this.check(TokenType.CLOSE_PAREN) && !this.isAtEnd) {
            do {
                params.push(this.consume(TokenType.IDENTIFIER, "Expect parameter name."));
            } while (this.match(TokenType.COMMA));
        }
        this.consume(TokenType.CLOSE_PAREN, "Expect ')' after parameters.");

        this.consume(TokenType.OPEN_CURLY, "Expect '{' before function body.");
        const body = this.block();
        return new LambdaExpr(params, body);
    }

    argsList() {
        const args: Expr[] = [];
        while (!this.check(TokenType.NL, TokenType.EOF, TokenType.CLOSE_CURLY) && !this.isAtEnd) {
            do {
                args.push(this.expression());
            } while (this.match(TokenType.COMMA));
        }
        this.consumeNL();
        return args;
    }

    binaryGenerator(next: () => Expr, ...types: TokenType[]) {
        next = next.bind(this);
        let expr = next();
        while (this.match(types)) {
            const op = this.previous();
            const right = next();
            expr = new BinaryExpr(expr, op, right);
        }
        return expr;
    }

    consumeNL() {
        if(this.isAtEnd) return undefined;
        if(this.check(TokenType.CLOSE_CURLY)) return undefined;
        return this.consume(TokenType.NL, "Expect newline after statement.")
    }

    consume(type: TokenType, message: string) {
        if (this.check(type)) return this.advance();

        throw this.errorHandler.tokenError(this.peek(), message);
    }

    match(type: TokenType | TokenType[], ...types: TokenType[]) {
        if (Array.isArray(type)) {
            types = type;
        } else {
            types.unshift(type);
        }
        if (this.check(types)) {
            this.advance();
            return true;
        }
    }

    check(type: TokenType | TokenType[], ...types: TokenType[]) {
        if (Array.isArray(type)) {
            types = type;
        } else {
            types.unshift(type);
        }
        if (this.isAtEnd) return false;
        return (types.includes(this.peek().type));
    }

    peek() {
        return this.tokens[this.current];
    }

    advance() {
        if (!this.isAtEnd) this.current++;
        return this.previous();
    }

    previous() {
        return this.tokens[this.current - 1];
    }

    synchronize() {
        this.advance();
        while (!this.isAtEnd) {
            switch (this.previous().type) {
                case TokenType.NL:
                case TokenType.CLOSE_CURLY:
                    return;
            }
            this.advance();
        }
    }
}