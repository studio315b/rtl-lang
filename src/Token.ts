export const enum TokenType {
    // keywords
    DEFAULT,
    DEFINE,
    DICTIONARY,
    ELSE,
    EXPORT,
    FUNC,
    IF,
    IMPORT,
    LET,
    LOOKUP,
    MODULE,
    RETURN,
    ROLL,
    TABLE,
    TYPE,
    WHILE,
    WEIGHTED,


    // primitives
    TRUE, FALSE, NUMBER, IDENTIFIER,

    // symbols
    OPEN_PAREN, CLOSE_PAREN, OPEN_CURLY, CLOSE_CURLY, OPEN_SQUARE, CLOSE_SQUARE,
    COLON, MINUS, PLUS, STAR, SLASH, DOT, D, AND, OR, COMMA, QUESTION,
    EQUALS, EQUALS_EQUALS,
    BANG, BANG_EQUALS,
    GREATER, GREATER_EQUALS,
    LESS, LESS_EQUALS,

    // string opts
    TICK, RAW_STRING, ESCAPE,

    // Terminators
    NL, EOF
}

export class Token {
    readonly type: TokenType;
    readonly lexeme: string;
    readonly literal: any;
    readonly file: string;
    readonly line: number;

    constructor(type: TokenType, lexeme: string, literal: any, file: string, line: number) {
        this.type = type;
        this.lexeme = lexeme;
        this.literal = literal;
        this.file = file;
        this.line = line;
    }

    toString() {
        return `${this.type} ${this.lexeme} ${this.literal}`;
    }
}