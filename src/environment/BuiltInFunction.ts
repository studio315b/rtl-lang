import { CallFn, Callable } from "./Callable";


export class BuiltInFunction extends Callable {
    readonly name: string;
    readonly call: CallFn;

    constructor(name: string, call: CallFn) {
        super();
        this.name = name;
        this.call = call;
    }

    toString() {
        return `<fn ${this.name}>`;
    }
}