import { LambdaExpr } from "../Expr";
import { execute } from "../Interpreter";
import { Token } from "../Token";
import { Return } from "../Types";
import { Callable } from "./Callable";
import { Environment } from "./Environment";

export class InterpretedFunction extends Callable {
    private readonly name: string = "anonymous";
    private readonly lambda: LambdaExpr;
    private readonly closure: Environment;

    constructor(lambda: LambdaExpr, closure: Environment, name?: Token) {
        super();
        this.lambda = lambda;
        this.closure = closure;
        if(name) this.name = name.lexeme;
    }

    call(args: any[]) {
        const env = new Environment(this.name, this.closure);
        args.forEach((v,i) => {env.define(this.lambda.params[i], v)});
        try {
            execute(this.lambda.body, env);
        } catch (e) {
            if(e instanceof Return) {
                return e.value;
            }
            throw e;
        }
    }

    toString() {
        return `<fn ${this.name}>`
    }

}