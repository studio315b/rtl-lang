import { CommandEntry, CommonEntry, Entry, RangeEntry, WeightedEntry } from "../Entry";
import { Expr, LiteralExpr } from "../Expr";
import { evaluateList } from "../Interpreter";
import { Token, TokenType } from "../Token";
import { Formatter, RuntimeError } from "../Types";
import { markdownFormatter } from "../formatters";
import { aAn } from "../utils/aAn";
import { xDy } from "../utils/dice";
import { BuiltInFunction } from "./BuiltInFunction";
import { Callable } from "./Callable";

function deepClone(value: any, env: Environment): any {
    switch(typeof(value)) {
        case "number":
        case "boolean":
            return value;
        case "string":
            return value.toString();
        default:
            if(value instanceof Environment) return value.clone(env);
            if(value instanceof Callable) return value;
            if(value instanceof Expr) return value;
            if(Array.isArray(value)) return value.map(v => deepClone(v, env));
    }
    console.info(`Value ${JSON.stringify(value)} was not cloneable`);
    return value;
}

export class Environment {
    private readonly name: string;
    private readonly enclosing: Environment | undefined;
    private readonly values: Record<string, any> = {}
    private readonly isTable: boolean;
    readonly entries: Entry[] = [];
    formatter: Formatter;
    lastIndex: number = 0;
    defaultResult: Expr[] = [new LiteralExpr("")];
    defaultRoll: Expr[] = [];
    type: TokenType = TokenType.WEIGHTED;

    get tables(): string[] {
        return Object.keys(this.values).filter(k => {
            let value = this.values[k];
            return value instanceof Environment && value.isTable;
        });
    }

    constructor(name: string, enclosing?: Environment, isTable: boolean = false) {
        this.name = name;
        this.isTable = isTable;
        this.enclosing = enclosing;
        this.formatter = this.enclosing?.formatter || markdownFormatter;
    }

    clone(newEnclose?: Environment) {
        const env = new Environment(this.name, newEnclose ?? this.enclosing, this.isTable);
        Object.keys(this.values).forEach(k => env.values[k] = deepClone(this.values[k], env));
        env.formatter = this.formatter;
        env.defaultResult = this.defaultResult;
        if(this.isTable) {
            env.lastIndex = this.lastIndex;
            env.defaultRoll = this.defaultRoll;
            env.type = this.type;
            this.entries.map(e=>env.addEntry(e));
        }
        return env;
    }

    copyInto(env: Environment) {
        Object.entries(this.values).forEach(([k, v]) => env.values[k] = v);
    }

    addBuiltIn(fn: BuiltInFunction) {
        this.values[fn.name] = fn;
    }

    addValue(name: string, value: any) {
        this.values[name] = value;
    }

    addEntry(entry: Entry) {
        if (entry instanceof CommandEntry) {
            switch (entry.keyword.type) {
                case TokenType.DEFAULT:
                    this.defaultResult = entry.value;
                    return;
                case TokenType.ROLL:
                    this.defaultRoll = entry.value;
                    return;
                default:
                    this.type = entry.keyword.type;
                    return;
            }
        }
        if (this.type == TokenType.LOOKUP) {
            if (entry instanceof WeightedEntry) this.lastIndex = entry.weight;
            if (entry instanceof RangeEntry) this.lastIndex = entry.max;
            if (entry instanceof CommonEntry) {
                this.lastIndex++;
                entry = new WeightedEntry(this.lastIndex, entry.value);
            }
        }
        this.entries.push(entry);
    }

    define(name: Token, value: any) {
        if (this.values[name.lexeme] != undefined) {
            throw new RuntimeError(name, `Variable "${name.lexeme}" already defined in this context.`);
        }
        this.values[name.lexeme] = value;
    }

    assign(name: Token, value: any) {
        if (this.values[name.lexeme] == undefined) {
            if (this.enclosing == undefined) {
                throw new RuntimeError(name, `Variable "${name.lexeme}" not defined in this context.`);
            }
            this.enclosing.assign(name, value);
        }
        this.values[name.lexeme] = value;
    }

    getLocalOnly(name: string) {
        return this.values[name];
    }

    get(name: Token): any {
        if (this.values[name.lexeme] == undefined) {
            if (this.enclosing == undefined) {
                throw new RuntimeError(name, `Undefined variable "${name.lexeme}".`);
            }
            return this.enclosing.get(name);
        }
        return this.values[name.lexeme];
    }

    roll(index?: any) {
        switch (this.type) {
            case TokenType.WEIGHTED:
                return this.rollWeighted(index);
            case TokenType.LOOKUP:
                return this.rollLookup(index);
            case TokenType.DICTIONARY:
                return this.rollDictionary(index);
        }
    }

    rollWeighted(index: any) {
        if (this.entries.length == 0) return evaluateList(this.defaultResult, this);
        if (index) return evaluateList((this.entries[index]?.value ?? this.defaultResult), this);
        const weights = this.entries.map(e => e.weight).reduce((p, c) => p + c);
        let x = xDy(1, weights);
        for (const entry of this.entries) {
            if (entry.weight >= x) {
                return evaluateList(entry.value, this);
            }
            x -= entry.weight;
        }
        return "";
    }

    rollLookup(index:any) {
        if(index == undefined) {
            index = evaluateList(this.defaultRoll, this);
        }
        for(const entry of this.entries) {
            if(entry.matches(index)) return evaluateList(entry.value, this);
        }
        return evaluateList(this.defaultResult, this);
    }

    rollDictionary(index:any) {
        if(index == undefined) {
            return evaluateList(this.defaultResult, this);
        }
        for(const entry of this.entries) {
            if(entry.matches(index)) return evaluateList(entry.value, this);
        }
        return evaluateList(this.defaultResult, this);
    }

    toString() {
        return `<${this.isTable? "table" : "module"} ${this.name}>`;
    }
}

export const mathModule = (() => {
    const math = new Environment("Math");
    // Duplicating JavaScript's Math namespace
    Object.getOwnPropertyNames(Math).forEach((k) => {
        let v = (Math as any)[k];
        switch(typeof(v)) {
            case "number":
                math.addValue(k, v);
                break;
            case "function":
                math.addBuiltIn(new BuiltInFunction(k, (args) => v(...args)))
                break;
        }
    })
    return math;
})();

export const globalEnv = (() => {
    const global = new Environment("global");
    global.addBuiltIn(new BuiltInFunction("clock", () => Date.now() / 1000))
    global.addBuiltIn(new BuiltInFunction("log", ([arg]) => console.info(arg)))
    global.addBuiltIn(new BuiltInFunction("parseNumber", ([arg]) => Number.parseFloat(arg)));
    global.addBuiltIn(new BuiltInFunction("isNaN", ([arg]) => Number.isNaN(arg)));
    global.addBuiltIn(new BuiltInFunction("aAn", ([arg]) => aAn(arg)))
    global.addValue("Math", mathModule);
    return global;
})();
