import { Environment } from "./Environment";
import { Token } from "../Token";
import { RuntimeError, TypeMatchError } from "../Types";
import { BuiltInFunction } from "./BuiltInFunction";
import { CallFn, Callable } from "./Callable";

type MethodDelegate<T> = (that:T) => CallFn;

class TypeMethods<T> {
    private readonly methods: Record<string, MethodDelegate<T>> = {}
    private readonly type: string;

    constructor(type: string) {
        this.type = type;
        this.methods["type"] = () => () => this.type
        this.methods["string"] = (v) => () => (v as any).toString();
    }

    has(name: Token) {
        return this.methods[name.lexeme] != undefined;
    }

    add(name: string, call: MethodDelegate<T>) {
        this.methods[name] = call;
    }

    get(name: Token, value: T) {
        if(!this.methods[name.lexeme]) {
            throw new RuntimeError(name, `No method called '${name.lexeme}' on type '${this.type}'.`)
        }
        return new BuiltInFunction(`${this.type}.${name.lexeme}`, this.methods[name.lexeme](value));
    }
}

export const arrayMethods = (() => {
    const arrayMethods = new TypeMethods<any[]>("array");
    arrayMethods.add("concat", (value: any[]) => (args)=> value.concat(...args));
    arrayMethods.add("every", (value: any[]) => ([v0], env)=> {
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        return value.every(v => v0.call([v], env));
    });
    arrayMethods.add("fill", (value: any[]) => ([v0,v1,v2])=> value.fill(v0,v1,v2));
    arrayMethods.add("filter", (value: any[]) => ([v0], env)=> {
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        return value.filter(v => v0.call([v], env));
    });
    arrayMethods.add("find", (value: any[]) => ([v0], env)=> {
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        return value.find(v => v0.call([v], env));
    });
    arrayMethods.add("findIndex", (value: any[]) => ([v0], env)=> {
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        return value.findIndex(v => v0.call([v], env));
    });
    arrayMethods.add("flat", (value: any[]) => ()=> value.flat());
    arrayMethods.add("flatMap", (value: any[]) => ([v0], env)=> {
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        return value.flatMap(v => v0.call([v], env));
    });
    arrayMethods.add("join", (value: any[]) => ([v0])=> value.join(v0));
    arrayMethods.add("map", (value: any[]) => ([v0], env)=> {
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        return value.map(v => v0.call([v], env));
    });
    arrayMethods.add("pop", (value: any[]) => ()=> value.pop());
    arrayMethods.add("push", (value: any[]) => ([v0])=> value.push(v0));
    arrayMethods.add("reduce", (value: any[]) => ([v0, v1], env)=> {
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        if(v1) return value.reduce((p,c,i,a) => v0.call([p,c,i,a], env), v1)
        return value.reduce((p,c,i,a) => v0.call([p,c,i,a], env))
    });
    arrayMethods.add("reverse", (value: any[]) => ()=> value.reverse());
    arrayMethods.add("shift", (value: any[]) => ()=> value.shift());
    arrayMethods.add("slice", (value: any[]) => ([v0,v1]) => value.slice(v0,v1));
    arrayMethods.add("some", (value: any[]) => ([v0], env)=> {
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        return value.some((v,i,a) => v0.call([v,i,a], env))
    });
    arrayMethods.add("sort", (value: any[]) => ([v0], env)=> {
        if(!v0) return value.sort();
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        return value.sort((a,b) => v0.call([a,b], env))
    });
    arrayMethods.add("unshift", (value: any[]) => ([v0])=> value.unshift());
    return arrayMethods;
})()

export const numberMethods = (()=> {
    const numberMethods = new TypeMethods<number>("number");
    numberMethods.add("fixed", (value: number) => ([v0]) => value.toFixed(v0 ? v0 : 1))
    numberMethods.add("precision", (value: number) => ([v0]) => value.toPrecision(v0));
    numberMethods.add("plusMinus", (value: number) => () => `${value < 0 ? '' : '+'}${value}`);
    return numberMethods;
})();

export const stringMethods = (()=>{
    const stringMethods = new TypeMethods<string>("string");
    stringMethods.add("bold", (value: string) => (_, env) => env.formatter.bold(value));
    stringMethods.add("concat", (value: string) => (args) => value.concat(...args));
    stringMethods.add("endsWith", (value: string) => ([v0,v1]) => value.endsWith(v0,v1));
    stringMethods.add("eachChar", (value:string) => ([v0], env) => {
        if(!(v0 instanceof Callable)) throw new TypeMatchError("Callable", typeof(v0));
        return Array.from(value).map(v => v0.call([v], env));
    });
    stringMethods.add("includes", (value:string) => ([v0,v1]) => value.includes(v0,v1));
    stringMethods.add("italic", (value:string) => (_,env) => env.formatter.italic(value));
    stringMethods.add("left", (value: string) => ([v0]) => value.slice(0,v0 ? v0 : 1))
    stringMethods.add("length", (value: string) => () => value.length)
    stringMethods.add("lower", (value: string) => () => value.toLowerCase())
    stringMethods.add("match", (value:string) => ([v0]) => value.match(new RegExp(v0)));
    stringMethods.add("matchAll", (value:string) => ([v0]) => value.matchAll(new RegExp(v0)));
    stringMethods.add("padEnd", (value: string) => ([v0, v1]) => value.padEnd(v0,v1));
    stringMethods.add("padStart", (value: string) => ([v0, v1]) => value.padStart(v0,v1));
    stringMethods.add("reverse", (value: string) => () => Array.from(value).reverse().join(""));
    stringMethods.add("repeat", (value: string) => ([v0]) => value.repeat(v0));
    stringMethods.add("replace", (value: string) => ([v0, v1]) => value.replace(v0, v1));
    stringMethods.add("replaceAll", (value: string) => ([v0, v1]) => value.replaceAll(v0, v1));
    stringMethods.add("right", (value: string) => ([v0]) => value.slice(value.length-(v0 ? v0 : 1)));
    stringMethods.add("startsWith", (value: string) => ([v0,v1]) => value.startsWith(v0,v1));
    stringMethods.add("split", (value: string) => ([v0]) => value.split(v0));
    stringMethods.add("slice", (value: string) => ([v0,v1]) => value.slice(v0,v1));
    stringMethods.add("trim", (value: string) => () => value.trim())
    stringMethods.add("trimStart", (value: string) => () => value.trimStart())
    stringMethods.add("trimEnd", (value: string) => () => value.trimEnd())
    stringMethods.add("upper", (value: string) => () => value.toUpperCase())
    return stringMethods;
})();