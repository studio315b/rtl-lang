import { Environment } from "./Environment";

export type CallFn = (args:any[], env: Environment) => any;

export abstract class Callable {
    abstract call(args: any[], env: Environment): any;
}